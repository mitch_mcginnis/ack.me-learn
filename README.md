#Ack.me Learn
The purpose of this repo is to teach the fundamentals of development with the Ack.me module.

Check out the [wiki](https://bitbucket.org/mitch_mcginnis/ack.me-learn/wiki/) for more information on how to do this.